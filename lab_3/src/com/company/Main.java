package com.company;

import com.company.entity.Archive;
import com.company.parser.DOMParser;

public class Main {
    public static void main(String [] args) {
        DOMParser domParser = new DOMParser();
        domParser.buildDrugSet("data/student.xml");
        Archive archive = new Archive();
        archive.setStudentList(domParser.getStudentList());
        System.out.println(archive);
    }
}
