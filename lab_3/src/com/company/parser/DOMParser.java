package com.company.parser;

import com.company.entity.Student;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DOMParser {
    private DocumentBuilder documentBuilder;
    private ArrayList<Student> students;

    public DOMParser() {
        this.students = new ArrayList<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Student> getStudentList() {
        return students;
    }
    public void buildDrugSet(final String fileName) {
        Document document;
        try {
            document = documentBuilder.parse(fileName);
            Element root = document.getDocumentElement();
            NodeList studentList = root.getElementsByTagName("student");
            for (int i = 0; i < studentList.getLength(); i++) {
                Element studentElement = (Element) studentList.item(i);
                Student student = buildStudent(studentElement);
                students.add(student);

            }
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    private Student buildStudent(Element studentElement) {
        Student student = new Student();
        student.setName(getElementTextContent(studentElement,"name"));
        student.setPhone(getElementTextContent(studentElement,"phone"));
        student.setCity(getElementTextContent(studentElement,"city"));
        return student;
    }

    private static String getElementTextContent(final Element element,
                                                final String elementName) {
        NodeList nodeList = element.getElementsByTagName(elementName);
        Node node = nodeList.item(0);
        return node.getTextContent();
    }

    public void updateXML(List<Student> studentList){
        Document document = documentBuilder.newDocument();

        Element root = document.createElement("students");
        for (Student aStudentList : studentList) {
            Element student = document.createElement("student");
            Element name = document.createElement("name");
            Element phone = document.createElement("phone");
            Element city = document.createElement("city");
            root.appendChild(student);
            name.appendChild(document.createTextNode(aStudentList.getName()));
            phone.appendChild(document.createTextNode(aStudentList.getPhone()));
            city.appendChild(document.createTextNode(aStudentList.getCity()));
            student.appendChild(name);
            student.appendChild(phone);
            student.appendChild(city);
        }
        document.appendChild(root);
        try {
            Transformer tr = TransformerFactory.newInstance().newTransformer();
            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "xml");
            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            // send DOM to file
            tr.transform(new DOMSource(document),
                    new StreamResult(new FileOutputStream("data/student.xml")));

        } catch (TransformerException | IOException te) {
            System.out.println(te.getMessage());
        }


    }
}
