package com.company.server.serveraction;

import com.company.constant.Constant;
import com.company.entity.Archive;
import com.company.parser.DOMParser;

import java.io.IOException;
import java.io.ObjectOutputStream;

public class ViewAction {
    public static void execute(ObjectOutputStream dos) throws IOException {
        DOMParser domParser = new DOMParser();
        domParser.buildDrugSet(Constant.XML_PATH);
        Archive archive = new Archive();
        archive.setStudentList(domParser.getStudentList());
        dos.writeObject(archive);
    }
}
