package com.company.server.serveraction;

import com.company.constant.Constant;
import com.company.entity.Archive;
import com.company.entity.Student;
import com.company.parser.DOMParser;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class AddAction {
    public static void execute(ObjectOutputStream dos, ObjectInputStream dis) throws IOException, ClassNotFoundException {
        Student student = (Student) dis.readObject();
        addStudent(student);
        dos.writeObject("user created" + student.toString());
    }

    private static void addStudent(Student student) {
        DOMParser domParser = new DOMParser();
        domParser.buildDrugSet(Constant.XML_PATH);
        Archive archive = new Archive();
        archive.setStudentList(domParser.getStudentList());
        archive.getStudentList().add(student);
        domParser.updateXML(archive.getStudentList());
    }
}
