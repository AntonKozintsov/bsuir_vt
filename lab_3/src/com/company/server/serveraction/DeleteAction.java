package com.company.server.serveraction;

import com.company.constant.Constant;
import com.company.entity.Archive;
import com.company.parser.DOMParser;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class DeleteAction {
    public static void execute(ObjectOutputStream dos, ObjectInputStream dis) throws IOException, ClassNotFoundException {
        String name = (String) dis.readObject();
        deleteStudent(name);
        dos.writeObject("client want to delete user with name " + name);
    }

    private static void deleteStudent(String name) {
        DOMParser domParser = new DOMParser();
        domParser.buildDrugSet(Constant.XML_PATH);
        Archive archive = new Archive();
        archive.setStudentList(domParser.getStudentList());
        archive.getStudentList().remove(archive.getIndexByName(name));
        domParser.updateXML(archive.getStudentList());
    }
}
