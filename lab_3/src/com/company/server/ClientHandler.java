package com.company.server;

import com.company.server.serveraction.AddAction;
import com.company.server.serveraction.DeleteAction;
import com.company.server.serveraction.ViewAction;

import java.io.*;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

class ClientHandler extends Thread {

    final ObjectOutputStream dos;
    final ObjectInputStream dis;
    final Socket s;


    public ClientHandler(Socket s, ObjectOutputStream dos, ObjectInputStream dis) {
        this.s = s;
        this.dos = dos;
        this.dis = dis;

    }

    @Override
    public void run() {
        String received;
        while (true) {
            try {

                // Ask user what he wants
                dos.writeObject("Enter your permissions [View | Add | Delete]..\n" +
                        "Type Exit to terminate connection.");

                // receive the answer from client
                received = (String) dis.readObject();

                if (received.equals("Exit")) {
                    System.out.println("Client " + this.s + " sends exit...");
                    System.out.println("Closing this connection.");
                    dos.flush();
                    this.s.close();
                    System.out.println("Connection closed");
                    break;
                }
                // answer from the client
                switch (received) {

                    case "View":
                        ViewAction.execute(dos);
                        break;
                    case "Add":
                        AddAction.execute(dos, dis);
                        break;
                    case "Delete":
                        DeleteAction.execute(dos, dis);
                        break;
                    default:
                        dos.writeObject("Invalid input");
                        break;
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        try {
            // closing resources
            this.dos.close();
            this.dis.close();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}