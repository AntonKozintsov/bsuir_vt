package com.company.client;

import com.company.client.clientAction.AddStudent;
import com.company.client.clientAction.DeleteStudent;
import com.company.client.clientAction.ViewStudent;
import com.company.entity.Student;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws IOException {
        try {
            Scanner scn = new Scanner(System.in);

            // getting localhost ip
            InetAddress ip = InetAddress.getByName("localhost");

            // establish the connection with server port 5056
            Socket s = new Socket(ip, 5056);

            // obtaining input and out streams
            ObjectOutputStream dos = new ObjectOutputStream(s.getOutputStream());
            ObjectInputStream dis = new ObjectInputStream(s.getInputStream());


            // the following loop performs the exchange of
            // information between client and client handler
            while (true) {
                String received = "";
                System.out.println(dis.readObject());
                String tosend = scn.nextLine();
                System.out.println(tosend);
                dos.writeObject(tosend);

                // If client sends exit,close this connection
                // and then break from the while loop
                if (tosend.equals("Exit")) {
                    System.out.println("Closing this connection : " + s);
                    dos.flush();
                    System.out.println("Connection closed");
                    break;
                }
                switch (tosend) {

                    case "View": {
                        ViewStudent.execute(dis);
                        break;
                    }
                    case "Delete": {
                        DeleteStudent.execute(scn, dos, dis);
                        break;
                    }
                    case "Add": {
                        AddStudent.execute(scn, dos, dis);
                        break;
                    }
                    case "Exit": {
                        System.out.println("Closing this connection : " + s);
                        dos.flush();
                        s.close();
                        System.out.println("Connection closed");
                        break;
                    }
                    default:
                        // printing date or time as requested by client
                        received = (String) dis.readObject();
                        System.out.println(received);
                        break;

                }

            }
            // closing resources
            scn.close();
            dos.flush();
            dos.close();
            dis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}