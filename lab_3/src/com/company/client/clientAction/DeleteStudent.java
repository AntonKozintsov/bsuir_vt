package com.company.client.clientAction;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class DeleteStudent {
    public static void execute(Scanner scn, ObjectOutputStream dos, ObjectInputStream dis) throws IOException, ClassNotFoundException {
        System.out.println("Enter student name to Delete");
        String name = scn.nextLine();
        dos.writeObject(name);
        String received = (String) dis.readObject();
        System.out.println(received);
    }
}
