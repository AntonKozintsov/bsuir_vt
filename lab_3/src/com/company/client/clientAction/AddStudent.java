package com.company.client.clientAction;

import com.company.entity.Student;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;


public class AddStudent {
    public static void execute(Scanner scn, ObjectOutputStream dos, ObjectInputStream dis) throws IOException, ClassNotFoundException {
        System.out.println("enter student Name");
        String name  = scn.nextLine();
        System.out.println("enter student Phone");
        String phone  = scn.nextLine();
        System.out.println("enter student CITY");
        String city  = scn.nextLine();
        Student student = new Student(name,phone,city);
        dos.writeObject(student);
        String received = (String) dis.readObject();
        System.out.println(received);
    }
}
