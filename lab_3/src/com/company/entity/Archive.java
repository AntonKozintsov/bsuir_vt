package com.company.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.stream.IntStream;

public class Archive implements Serializable {
    ArrayList<Student> studentList;


    public Archive() {
    }

    public ArrayList<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(ArrayList<Student> studentList) {
        this.studentList = studentList;
    }

    public int getIndexByName(String name){
        return IntStream.range(0, studentList.size())
                .filter(i -> name.equals(studentList.get(i).getName()))
                .findFirst()
                .orElseThrow(()->new RuntimeException("Student was not found."));
    }

    @Override
    public String toString() {
        return "Archive{" +
                "studentList=" + studentList +
                '}';
    }
}
