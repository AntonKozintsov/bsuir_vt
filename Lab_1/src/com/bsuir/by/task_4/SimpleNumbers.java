package com.bsuir.by.task_4;

import java.util.ArrayList;
import java.util.List;

public class SimpleNumbers {
    /**
     * @param arr array of simple numbers
     * @return indexes
     */
    public static List<Integer> getIndexes(int [] arr) throws NullPointerException {
        List<Integer> indexes = new ArrayList<>();
        for (int i=0;i<arr.length;i++) {
            if (isPrime(arr[i])) {
                indexes.add(i);
            }
        }
        return indexes;
    }

    /**
     * Method to check number is prime
     * @param number number
     * @return boolean value
     */
   private static boolean isPrime(int number) {
        if (number==2) {
            return false;
        }
       for (int i = 2; i < number; i++) {
           if (number % i == 0)
               return false;
       }
       return true;
   }
}
