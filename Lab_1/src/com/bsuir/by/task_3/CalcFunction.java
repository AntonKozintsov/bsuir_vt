package com.bsuir.by.task_3;

public class CalcFunction {
    /**
     * @param a number a
     * @param b number b
     * @param h number h
     * @return 2D double array
     */
    public static double[][] calc(double a, double b, double h) {
        int tableSize = getTableSize(a, b, h);
        System.out.println(tableSize);
        double value;
        double[][] resultTable = new double[tableSize][2];
        int i = 0;
        while (i < tableSize) {
            value = Math.tan(a);
            resultTable[i][0] = a;
            resultTable[i][1] = value;
            a += h;
            i++;
        }
        return resultTable;
    }

    /**
     * @param a number a
     * @param b number b
     * @param h number h
     * @return table size
     */
    private static int getTableSize(double a, double b, double h) {
        int size = 0;
        for (double i = a; i <= b; i += h) {
            size++;
        }
        return size;
    }
}
