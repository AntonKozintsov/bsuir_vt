package com.bsuir.by.task_12;

import java.util.Objects;

public class Book implements Cloneable, Comparable<Book> {
    /**
     * title.
     */
    private String title;
    /**
     * author.
     */
    private String author;
    /**
     * price.
     */
    private int price;
    /**
     * isbn.
     */
    private int isbn;
    /**
     * {@inheritDoc}
     */
    public Book() {
    }
    public Book(String title, String author, int price,int isbn) {
        this.title = title;
        this.author = author;
        this.price = price;
        this.isbn = isbn;
    }
    /**
     * {@inheritDoc}
     */
    public String getTitle() {
        return title;
    }
    /**
     * {@inheritDoc}
     */
    public String getAuthor() {
        return author;
    }
    /**
     * {@inheritDoc}
     */
    public int getPrice() {
        return price;
    }

    /**
     * {@inheritDoc}
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * {@inheritDoc}
     */
    public void setAuthor(String author) {
        this.author = author;
    }
    /**
     * {@inheritDoc}
     */
    public void setPrice(int price) {
        this.price = price;
    }
    /**
     * {@inheritDoc}
     */
    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    /**
     * {@inheritDoc}
     */
    public int getIsbn() {
        return isbn;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return price == book.price &&
                isbn == book.isbn &&
                Objects.equals(title, book.title) &&
                Objects.equals(author, book.author);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(title, author, price);
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", price=" + price +
                ", isbn=" + isbn +
                '}';
    }
    /**
     * {@inheritDoc}
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(Book o) {
        return Integer.compare(this.isbn,o.isbn);
    }
}
