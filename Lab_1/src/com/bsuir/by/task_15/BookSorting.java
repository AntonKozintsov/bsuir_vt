package com.bsuir.by.task_15;

import com.bsuir.by.task_12.Book;

import java.util.Comparator;

public class BookSorting {
    public static Comparator<Book> byTitle = Comparator.comparing(Book::getTitle);
    public static Comparator<Book> byTitleThenAuthor = Comparator.comparing(Book::getTitle).thenComparing(Book::getAuthor);
    public static Comparator<Book> byAuthorThenTitle = Comparator.comparing(Book::getAuthor).thenComparing(Book::getTitle);
    public static Comparator<Book> byAuthorTitlePrice = Comparator
            .comparing(Book::getAuthor)
            .thenComparing(Book::getTitle)
            .thenComparing(Book::getPrice);

}
