package com.bsuir.by.task_13;

import com.bsuir.by.task_12.Book;

import java.util.Objects;

public class ProgrammerBook extends Book {
    /**
     * language.
     */
    private String language;
    /**
     * level.
     */
    private int level;
    /**
     * {@inheritDoc}
     */
    public ProgrammerBook(String title, String author, int price, int isbn, String language, int level) {
        super(title, author, price,isbn);
        this.language = language;
        this.level = level;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProgrammerBook)) return false;
        if (!super.equals(o)) return false;
        ProgrammerBook prBook = (ProgrammerBook) o;
        return level == prBook.level &&
                Objects.equals(language, prBook.language);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), language, level);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return super.toString()+" "+"language:"+language+" "+"level:"+level;
    }
}
