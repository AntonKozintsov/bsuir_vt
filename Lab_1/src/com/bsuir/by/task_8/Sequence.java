package com.bsuir.by.task_8;

public class Sequence {
    /**
     * @param a array a
     * @param b array b
     * @return sequence indexes
     */
    public static int[] readArr(double[] a, double[] b) throws NullPointerException{
        int[] result = new int[b.length];
        for(int i = 0; i<b.length; i++){
            int j = 0;
            while(j<a.length && a[j]<=b[i]){
                j++;
            }
            result[i] = j;
        }
        return result;
    }
}
