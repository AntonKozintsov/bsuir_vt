package com.bsuir.by.task_9;

import com.bsuir.by.task_9.entity.Ball;
import com.bsuir.by.task_9.entity.Basket;
import com.bsuir.by.task_9.entity.Color;

public class RunBall {
    public static void main(String[] args) {
        Ball ball = new Ball(Color.BLUE, 10);
        Ball ball3 = new Ball(Color.BLUE, 25);
        Ball ball1 = new Ball(Color.GREEN, 20);
        Basket basket = new Basket();
        basket.addBall(ball);
        basket.addBall(ball1);
        basket.addBall(ball3);
        System.out.println("before delete"+basket);
        basket.removeBall(new Ball(Color.BLUE, 10));
        System.out.println("after delete"+basket);
//        System.out.println(basket.calcWeight());
//        System.out.println(basket.getColorAmount(Color.BLUE));
//        System.out.println(basket);
    }
}
