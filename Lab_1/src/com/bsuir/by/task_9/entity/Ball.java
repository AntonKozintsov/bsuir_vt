package com.bsuir.by.task_9.entity;

import java.util.Objects;

public class Ball {
    /**
     * Ball Color.
     */
    private Color color;
    /**
     * Ball weight.
     */
    private double weight;

    /**
     * {@inheritDoc}
     */
    public Ball(Color color, double weight) {
        this.color = color;
        this.weight = weight;
    }
    /**
     * {@inheritDoc}
     */
    public Color getColor() {
        return color;
    }
    /**
     * {@inheritDoc}
     */
    public void setColor(Color color) {
        this.color = color;
    }
    /**
     * {@inheritDoc}
     */
    public double getWeight() {
        return weight;
    }
    /**
     * {@inheritDoc}
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Ball{" +
                "color=" + color +
                ", weight=" + weight +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ball)) return false;
        Ball ball = (Ball) o;
        return Double.compare(ball.weight, weight) == 0 &&
                color == ball.color;
    }
}
