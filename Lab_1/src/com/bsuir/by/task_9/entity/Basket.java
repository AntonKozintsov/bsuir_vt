package com.bsuir.by.task_9.entity;

import java.util.ArrayList;

public class Basket {
    /**
     * list of balls.
     */
    private ArrayList<Ball> ballList = new ArrayList<>();

    /**
     * default constructor
     */
    public Basket () {
    }

    /**
     * Method to add ball to a basket
     * @param ball ball
     */
    public void addBall(Ball ball) {
        if (ball!=null) {
            ballList.add(ball);
        }else {
            throw new NullPointerException("ball is null");
        }
    }

    /**
     * Method to calc weight
     * @return weight of basket
     */
    public double calcWeight() {
        double weight = 0;
        for (Ball ball : ballList) {
            weight += ball.getWeight();
        }
        return weight;
    }
    public void removeBall(Ball ball) {
        ballList.remove(ball);
    }


    /**
     * Method to get amount of colored balls
     * @param color color
     * @return amount of colored balls
     */
    public int getColorAmount(Color color) {
        int amount = 0;
        for (Ball ball : ballList) {
            if (ball.getColor() == color) {
                amount++;
            }
        }
        return amount;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Basket{" +
                "ballList=" + ballList +
                '}';
    }
}
