package com.bsuir.by.task_1;

public class CalcExpression {
    /**
     * @param x number
     * @param y number
     * @return calculated expression
     */
    public static double calculate(double x, double y) {
        double numerator = 1 + Math.pow(2,Math.sin(x+y));
        double denominator  = 2 + Math.abs(x - (2*x/1+Math.pow(2,x)*Math.pow(2,y)));
        return Math.round(numerator/denominator + x);
    }
}
