package com.bsuir.by.task_7;

public class SortShell {
    /**
     * Method to sort array
     * @param arr input array
     * @return sorted array
     */
    public static double[] sort(double[] arr) throws NullPointerException{

        int size = arr.length;
        int step = size / 2;//инициализируем шаг. while (step > 0)//пока шаг не 0
        while(step>0)
        {
            for (int i = 0; i < (size - step); i++) {
                int j = i; //будем идти начиная с i-го элемента while (j >= 0 && a[j] > a[j + step])
//пока не пришли к началу массива
//и пока рассматриваемый элемент больше
//чем элемент находящийся на расстоянии шага
                while(j >= 0 && arr[j]> arr[j+ step])
                {
//меняем их местами
                    double temp = arr[j];
                    arr[j] = arr[j + step];
                    arr[j + step] = temp;
                    j--;
                }
            }
            step = step / 2;
        }
        return arr;
    }
}
