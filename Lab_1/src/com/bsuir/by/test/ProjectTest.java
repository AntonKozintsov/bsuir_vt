package com.bsuir.by.test;

import com.bsuir.by.task_1.CalcExpression;
import com.bsuir.by.task_12.Book;
import com.bsuir.by.task_15.BookSorting;
import com.bsuir.by.task_2.ValidateCoordinate;
import com.bsuir.by.task_3.CalcFunction;
import com.bsuir.by.task_4.SimpleNumbers;
import com.bsuir.by.task_5.DropElements;
import com.bsuir.by.task_6.GenerateMatrix;
import com.bsuir.by.task_7.SortShell;
import com.bsuir.by.task_8.Sequence;
import com.bsuir.by.task_9.entity.Ball;
import com.bsuir.by.task_9.entity.Basket;
import com.bsuir.by.task_9.entity.Color;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.Collections;

public class ProjectTest {
    @DataProvider(name = "task_1")
    public static Object[][] task1() {
        return new Object[][]{{128.43, 534.4, 128}};
    }

    @DataProvider(name = "task_2")
    public static Object[][] task2() {
        return new Object[][]{{6, 0, true}, {-5, 1, false}};
    }

    @DataProvider(name = "task_3")
    public static Object[][] task3() {
        return new Object[][]{{0, 1.5574077246549023}, {1, 1.1578212823495775}, {2, 0.8714479827243188}, {3, 0.6483608274590866}};
    }

    @DataProvider(name = "task_4")
    public static Object[][] task4() {
        int[] arr = {2, 4, 6, 17, 13, 25};
        ArrayList<Integer> indexes = new ArrayList<>();
        indexes.add(3);
        indexes.add(4);
        return new Object[][]{{arr, indexes}};
    }

    @DataProvider(name = "task_5")
    public static Object[][] task5() {
        int[] arr = {1, 2, 3, 4, 5, 2, 1, 3, 4, 6, 4, 10};
        int amount = 5;
        return new Object[][]{{arr, amount}};
    }
    @DataProvider(name = "task_6")
    public static Object[][] task6(){
        double[] arr = {1, 2, 3, 4};
        double[][] expected = {{1,2,3,4},{2,3,4,1},{3,4,1,2},{4,1,2,3}};
        return new Object[][]{{arr,expected}};
    }

    @DataProvider(name = "task_7")
    public static Object[][] task7(){
        double[] arr = {15, 1, 28, 19, 4, 6};
        double[] expected = {1,4,6,15, 19,28};
        return new Object[][]{{arr,expected}};
    }

    @DataProvider(name = "task_8")
    public static Object[][] task8(){
        double[] a = {3,6,8,15,32};
        double[] b = {1,2,5,12,17,28,65,72};
        int[] expected = {0,0,1,3,4,4,5,5};
        return new Object[][]{{a,b,expected}};
    }

    @DataProvider(name = "task_9")
    public static Object[][] weight() {
        Basket basket = new Basket();
        basket.addBall(new Ball(Color.BLUE, 20));
        basket.addBall(new Ball(Color.GREEN, 15));
        return new Object[][]{{basket, 35.0}};
    }

    @DataProvider(name = "task_9.1")
    public static Object[][] count() {
        Basket basket = new Basket();
        basket.addBall(new Ball(Color.BLUE, 20));
        basket.addBall(new Ball(Color.GREEN, 15));
        basket.addBall(new Ball(Color.BLUE, 10));
        return new Object[][]{{basket, 2}};
    }
    @DataProvider(name = "task_14")
    public static Object[][] sorted() {
        ArrayList<Book> bookList = new ArrayList<>();
        bookList.add(new Book("book1","author1",10,1));
        bookList.add( new Book("book2","author2",20,7));
        bookList.add( new Book("book2","author2",20,4));
        ArrayList<Book> sortedCollection = new ArrayList<>();
        sortedCollection.add(new Book("book1","author1",10,1));
        sortedCollection.add( new Book("book2","author2",20,4));
        sortedCollection.add( new Book("book2","author2",20,7));
        return new Object[][]{{bookList,sortedCollection}};
    }
    @DataProvider(name="task_15")
    public static Object[][] sortByTitle() {
        ArrayList<Book> bookList = new ArrayList<>();
        bookList.add( new Book("book3","author3",20,7));
        bookList.add(new Book("book1","author1",15,1));
        bookList.add( new Book("book2","author2",20,4));
        ArrayList<Book> sortedCollection = new ArrayList<>();
        sortedCollection.add(new Book("book1","author1",15,1));
        sortedCollection.add( new Book("book2","author2",20,4));
        sortedCollection.add( new Book("book3","author3",20,7));
        return new Object[][]{{bookList,sortedCollection}};
    }
    @DataProvider(name="task_15.1")
    public static Object[][] sortByNameThenAuthor() {
        ArrayList<Book> bookList = new ArrayList<>();
        bookList.add( new Book("book3","author1",20,7));
        bookList.add(new Book("book1","author1",10,1));
        bookList.add( new Book("book2","author2",20,4));
        ArrayList<Book> sortedCollection = new ArrayList<>();
        sortedCollection.add(new Book("book1","author1",10,1));
        sortedCollection.add( new Book("book2","author2",20,4));
        sortedCollection.add( new Book("book3","author1",20,7));
        return new Object[][]{{bookList,sortedCollection}};
    }
    @DataProvider(name="task_15.2")
    public static Object[][] sortByAuthorNamePrice() {
        ArrayList<Book> bookList = new ArrayList<>();
        bookList.add( new Book("book1","author1",200,1));
        bookList.add(new Book("book3","author1",70,4));
        bookList.add( new Book("book2","author2",50,7));
        ArrayList<Book> sortedCollection = new ArrayList<>();
        sortedCollection.add(new Book("book1","author1",200,1));
        sortedCollection.add( new Book("book3","author1",70,4));
        sortedCollection.add( new Book("book2","author2",50,7));
        return new Object[][]{{bookList,sortedCollection}};
    }


    @Test(dataProvider = "task_1")
    public void testCalculate(double x, double y, double result) {
        Assert.assertEquals(CalcExpression.calculate(x, y), result);
    }

    @Test(dataProvider = "task_2")
    public void testCoordinate(double coordX, double coordY, boolean result) {
        Assert.assertEquals(ValidateCoordinate.validate(coordX, coordY), result);
    }

    @Test(dataProvider = "task_3")
    public void testFunction(int position, double func) {
        double[][] resultTable = CalcFunction.calc(1, 10, 3);
        Assert.assertEquals(resultTable[position][1], func);
    }

    @Test(dataProvider = "task_4")
    public void testPrimeNumbers(int[] arr, ArrayList<Integer> indexes) {
        Assert.assertEquals(SimpleNumbers.getIndexes(arr), indexes);
    }

    @Test(dataProvider = "task_5")
    public void testCount(int[] arr, int amount) {
        Assert.assertEquals(DropElements.countElements(arr), amount);

    }
    @Test(dataProvider = "task_6")
    public void testGenerateMatrix(double[] arr, double[][] expected){
        Assert.assertEquals(GenerateMatrix.generate(arr),expected);
    }

    @Test(dataProvider = "task_7")
    public void testSortShell(double[]arr, double[]expected){
        Assert.assertEquals(SortShell.sort(arr),expected);
    }

    @Test(dataProvider = "task_8")
    public void testSequence(double[] a, double[] b, int[] expected){
        Assert.assertEquals(Sequence.readArr(a,b),expected);
    }

    @Test(dataProvider = "task_9")
    public void testBasketWeight(Basket basket, double weight) {
        Assert.assertEquals(basket.calcWeight(), weight);
    }
    @Test(dataProvider = "task_9.1")
    public void testColorAmount(Basket basket, int amount) {
        Assert.assertEquals(basket.getColorAmount(Color.BLUE), amount);
    }
    @Test(dataProvider = "task_14")
    public void testBookSorting(ArrayList<Book> bookList,ArrayList<Book> sortedCollection) {
        Collections.sort(bookList);
        Assert.assertEquals(bookList,sortedCollection);
    }
    @Test(dataProvider = "task_15")
    public void testSortByTitle(ArrayList<Book> bookList,ArrayList<Book> sortedCollection) {
        bookList.sort(BookSorting.byTitle);
        Assert.assertEquals(bookList,sortedCollection);
    }
    @Test(dataProvider = "task_15.1")
    public void testSortByTitleThenAuthor(ArrayList<Book> bookList,ArrayList<Book> sortedCollection) {
        bookList.sort(BookSorting.byTitleThenAuthor);
        Assert.assertEquals(bookList,sortedCollection);
    }
    @Test(dataProvider = "task_15.2")
    public void testSortByAuthorNamePrice(ArrayList<Book> bookList,ArrayList<Book> sortedCollection) {
        bookList.sort(BookSorting.byAuthorTitlePrice);
        Assert.assertEquals(bookList,sortedCollection);

    }


}