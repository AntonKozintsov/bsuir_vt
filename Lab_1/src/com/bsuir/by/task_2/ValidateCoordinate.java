package com.bsuir.by.task_2;

public class ValidateCoordinate {
    /**
     * @param coordX coordinate x
     * @param coordY coordinate y
     * @return boolean result
     */
    public static boolean validate(double coordX, double coordY) {
        return validateNegative(coordX,coordY) || validatePositive(coordX, coordY);
    }

    /**
     * Method to validate negative numbers
     * @param coordX coordinate x
     * @param coordY coordinate y
     * @return boolean result
     */
    private static boolean validateNegative(double coordX, double coordY) {
        return (coordY >= -3 && coordY <=0 && coordX>=-6 && coordX<=6);
    }

    /**
     * Method to validate positive number
     * @param coordX coordinate x
     * @param coordY coordinate y
     * @return boolean result
     */
    private static boolean validatePositive(double coordX, double coordY) {
        return  (coordY <= 5 && coordY >= 0 && coordX>=-4 && coordX<=4);
    }

}
