package com.bsuir.by.task_6;

import java.io.FileNotFoundException;

public class GenerateMatrix {
    /**
     * Method to generate matrix
     * @param arr input values
     * @return resulted matrix
     */
    public static double[][] generate(double[] arr)  {
        double[][] result = new double[arr.length][arr.length];
        result[0] = arr;
        for (int i = 1; i < arr.length; i++) {
            for (int j = 0; j < arr.length-1; j++) {
                result[i][j] = result[i-1][j+1];
            }
            result[i][arr.length-1] = result[i-1][0];
        }
        return result;
    }
}
