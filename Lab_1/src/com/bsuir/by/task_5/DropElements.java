package com.bsuir.by.task_5;

public class DropElements {
    /**
     * Method to count elements
     * @param arr input array
     * @return result
     */
    public static int countElements(int[] arr) throws  NullPointerException  {
        int result = 0;
        int max = arr[0];
        for(int i = 0; i < arr.length - 1; i++) {
            if (arr[i + 1] <= max) {
                result++;
            } else {
                max = arr[i + 1];
            }
        }
        return result;
    }
}
