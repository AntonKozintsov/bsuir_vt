package com.epam.by.util;

import com.epam.by.entity.Role;

public class ValidateRole {
    public static boolean validate(Role role) {
        return role == Role.ADMINISTRATOR;
    }
}
