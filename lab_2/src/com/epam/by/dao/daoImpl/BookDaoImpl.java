package com.epam.by.dao.daoImpl;

import com.epam.by.constant.Constant;
import com.epam.by.dao.Dao;
import com.epam.by.fileAction.CreateEntity;
import com.epam.by.entity.Book;
import com.epam.by.fileAction.WriteFile;

import java.util.List;

public class BookDaoImpl implements Dao<Book> {
    private List<Book> bookList = CreateEntity.getBookList();

    public Book getBook(int id) {
        Book book = new Book();
        for (Book aBookList : bookList) {
            if (aBookList.getId() == id) {
                book = aBookList;
            }
        }
        return book;

    }

    @Override
    public List<Book> getAll() {
        bookList = CreateEntity.getBookList();
        return bookList;
    }

    @Override
    public void add(Book book) {
        WriteFile.writeInfo(Constant.BOOK_FILE, book);
        bookList = CreateEntity.getBookList();
    }

    @Override
    public void delete(int id) {
        WriteFile.deleteInfo(Constant.BOOK_FILE,getBook(id));
        bookList = CreateEntity.getBookList();
    }


    @Override
    public void update() {

    }

}
