package com.epam.by.dao.daoImpl;

import com.epam.by.dao.Dao;
import com.epam.by.fileAction.CreateEntity;
import com.epam.by.fileAction.WriteFile;
import com.epam.by.constant.Constant;
import com.epam.by.entity.User;

import java.util.List;
import java.util.Optional;

public class UserDaoImpl implements Dao<User> {
    private List<User> userList = CreateEntity.getUserList();


    public User getUser(int id) {
        User user = new User();
        for (User anUserList : userList) {
            if (anUserList.getId() == id) {
                user = anUserList;
            }
        }
        return user;
    }

    @Override
    public List<User> getAll() {
        userList = CreateEntity.getUserList();
        return userList;
    }

    @Override
    public void add(User user) {
        WriteFile.writeInfo(Constant.USER_FILE, user);
        userList = CreateEntity.getUserList();
    }

    @Override
    public void delete(int id) {
        WriteFile.deleteInfo(Constant.USER_FILE, getUser(id));
        userList = CreateEntity.getUserList();
    }

    @Override
    public void update() {
        try {
            throw new Exception("This method not need yet");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
