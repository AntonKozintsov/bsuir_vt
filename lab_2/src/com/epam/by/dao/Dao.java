package com.epam.by.dao;

import com.epam.by.entity.Book;
import com.epam.by.entity.User;

import java.util.List;
import java.util.Optional;

public interface Dao <T> {
    List<T> getAll();
    void add(T t);
    void delete(int id);
    void update();
}
