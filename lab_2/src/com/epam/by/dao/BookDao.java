package com.epam.by.dao;

import com.epam.by.entity.Book;

public interface BookDao extends Dao<Book> {
    void addBook(Book book);
    
}
