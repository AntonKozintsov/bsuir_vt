package com.epam.by.entity;

public enum Role {
    ADMINISTRATOR,
    USER;
}
