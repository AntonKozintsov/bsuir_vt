package com.epam.by.entity;

import java.util.Objects;

public class Book {
    private int id;
    private String title;
    private String author;
    private BookType bookType;

    public Book() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public BookType getBookType() {
        return bookType;
    }

    public void setBookType(BookType bookType) {
        this.bookType = bookType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return id == book.id &&
                Objects.equals(title, book.title) &&
                Objects.equals(author, book.author) &&
                bookType == book.bookType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, author, bookType);
    }

    @Override
    public String toString() {
        return id+"  "+title+"  "+author+"  "+String.valueOf(bookType).toLowerCase();
    }
}
