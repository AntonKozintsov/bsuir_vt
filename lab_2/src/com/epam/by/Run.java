package com.epam.by;

import com.epam.by.action.*;
import com.epam.by.constant.Constant;
import com.epam.by.dao.Dao;
import com.epam.by.dao.daoImpl.BookDaoImpl;
import com.epam.by.dao.daoImpl.UserDaoImpl;
import com.epam.by.entity.Book;
import com.epam.by.entity.BookType;
import com.epam.by.entity.Role;
import com.epam.by.entity.User;
import com.epam.by.fileAction.CreateEntity;
import com.epam.by.fileAction.ReadFile;
import com.epam.by.fileAction.WriteFile;
import com.epam.by.util.HashPassword;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Run {
    private static User user;

    public static void main(String[] args) {
//        List<User> userList = CreateEntity.getUserList();
//        System.out.println(userList);
        run();
    }

    public static void run() {
        System.out.println("Enter action you want to do");
        Scanner scanner = new Scanner(System.in);
        String action = scanner.next();
        doAction(action);
    }

    private static void doAction(String action) {
        UserAction userAction;
        switch (action) {
            case "login":
                user = new User();
                userAction = new Authorization();
                userAction.doAction(user);
                run();
                break;
            case "logout":
                break;
            case "viewusers":
                userAction = new ViewUser();
                userAction.doAction(user);
                break;
            case "adduser":
                userAction = new AddUser();
                userAction.doAction(user);
                break;
            case "deleteuser":
                userAction = new DeleteUser();
                userAction.doAction(user);
                break;
            case "viewbook":
                userAction = new ViewBook();
                userAction.doAction(user);
                break;
            case "addbook":
                userAction = new AddBook();
                userAction.doAction(user);
                break;
            case "deletebook":
                userAction = new DeleteBook();
                userAction.doAction(user);
                break;
            default:
                run();
        }
    }
}
