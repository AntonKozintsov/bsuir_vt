package com.epam.by.action;

import com.epam.by.Run;
import com.epam.by.dao.Dao;
import com.epam.by.dao.daoImpl.BookDaoImpl;
import com.epam.by.entity.Book;
import com.epam.by.entity.User;

public class ViewBook implements UserAction{

    @Override
    public void doAction(User user) {
        Dao<Book> bookDao = new BookDaoImpl();
        System.out.println(bookDao.getAll());
        Run.run();

    }
}
