package com.epam.by.action;

import com.epam.by.entity.Role;

public class ValidateRole {
    public static boolean validate(Role role) {
        return role == Role.ADMINISTRATOR;
    }
}
