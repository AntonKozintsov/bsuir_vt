package com.epam.by.action;

import com.epam.by.Run;
import com.epam.by.dao.Dao;
import com.epam.by.dao.daoImpl.UserDaoImpl;
import com.epam.by.entity.User;
import com.epam.by.util.ValidateRole;

public class ViewUser implements UserAction {
    @Override
    public void doAction(User user) {
        Dao<User> dao;
        if (ValidateRole.validate(user.getRole())) {
            dao = new UserDaoImpl();
            System.out.println(dao.getAll());
            Run.run();
        }else {
            System.out.println("not enough permissions");
            Run.run();
        }
    }
}
