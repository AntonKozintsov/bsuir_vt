package com.epam.by.action;

import com.epam.by.Run;
import com.epam.by.dao.Dao;
import com.epam.by.dao.daoImpl.UserDaoImpl;
import com.epam.by.entity.User;
import com.epam.by.util.ValidateRole;

import java.util.Scanner;

public class DeleteUser implements UserAction {
    @Override
    public void doAction(User user) {
        Dao<User> userDao = new UserDaoImpl();
        Scanner scanner = new Scanner(System.in);
        if (ValidateRole.validate(user.getRole())) {
            System.out.println("enter user id to delete");
            int id = scanner.nextInt();
            System.out.println("book ["+((UserDaoImpl) userDao).getUser(id)+"] will be deleted");
            userDao.delete(id);
            Run.run();
        } else {
            System.out.println("not enough permissions");
            Run.run();
        }
    }
}
