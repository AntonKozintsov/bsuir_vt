package com.epam.by.action;

import com.epam.by.Run;
import com.epam.by.dao.Dao;
import com.epam.by.dao.daoImpl.BookDaoImpl;
import com.epam.by.dao.daoImpl.UserDaoImpl;
import com.epam.by.entity.Book;
import com.epam.by.entity.BookType;
import com.epam.by.entity.User;
import com.epam.by.util.SendEmail;
import com.epam.by.util.ValidateRole;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AddBook implements UserAction {
    @Override
    public void doAction(User user) {
        Dao<Book> dao = new BookDaoImpl();
        if (ValidateRole.validate(user.getRole())) {
            Book addingBook = new Book();
            Scanner scanner = new Scanner(System.in);
            System.out.println("enter id");
            addingBook.setId(scanner.nextInt());
            System.out.println("enter title");
            addingBook.setTitle(scanner.next());
            System.out.println("enter author");
            addingBook.setAuthor(scanner.next());
            System.out.println("enter type[e_book,paper_book]");
            if (scanner.next().equals("e_book")){
                addingBook.setBookType(BookType.E_BOOK);
            }else if (scanner.next().equals("paper_book")) {
                addingBook.setBookType(BookType.PAPER_BOOK);
            }
            dao.add(addingBook);
            UserDaoImpl userDao = new UserDaoImpl();
            List<User> userList = userDao.getAll();
            for (int i=0;i<userList.size();i++) {
                SendEmail.send(userList.get(i).getEmail(),addingBook.toString()+"was added");
            }

            System.out.println("book ["+addingBook.toString()+"] successfully added");
            Run.run();
        }else {
            System.out.println("not enough permissions");
            Run.run();
        }
    }
}
