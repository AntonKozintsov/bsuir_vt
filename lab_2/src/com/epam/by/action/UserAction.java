package com.epam.by.action;

import com.epam.by.entity.User;

public interface UserAction {
    void doAction(User user);
}
