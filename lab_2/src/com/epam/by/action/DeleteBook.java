package com.epam.by.action;

import com.epam.by.Run;
import com.epam.by.dao.Dao;
import com.epam.by.dao.daoImpl.BookDaoImpl;
import com.epam.by.entity.Book;
import com.epam.by.entity.User;
import com.epam.by.util.ValidateRole;

import java.util.Scanner;

public class DeleteBook implements UserAction {
    @Override
    public void doAction(User user) {
        Dao<Book> bookDao = new BookDaoImpl();
        Scanner scanner = new Scanner(System.in);
        if (ValidateRole.validate(user.getRole())) {
            System.out.println("enter book id to delete");
            int id = scanner.nextInt();
            System.out.println("book ["+((BookDaoImpl) bookDao).getBook(id)+"] will be deleted");
            bookDao.delete(id);
            Run.run();
        } else {
            System.out.println("not enough permissions");
            Run.run();
        }
    }
}
