
package com.epam.by.action;

/**
 * class to generate ID.
 */
public final class GenerateId {
    /**
     * id counter.
     */
    private static int userIdCounter;
    private static int bookIdCounter;

    /**
     * private constructor.
     */
    private GenerateId() {
    }

    /**
     * @return increment userId
     */
    public static int createUserId() {
        return userIdCounter++;
    }

    /**
     * @return increment bookId
     */
    public static int createBookId() {
        return bookIdCounter++;
    }
}