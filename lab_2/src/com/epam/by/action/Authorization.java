package com.epam.by.action;

import com.epam.by.Run;
import com.epam.by.dao.Dao;
import com.epam.by.dao.daoImpl.UserDaoImpl;
import com.epam.by.entity.User;
import com.epam.by.util.HashPassword;

import java.util.List;
import java.util.Scanner;

public class Authorization implements UserAction {

    @Override
    public void doAction(User user) {
        System.out.println("Enter login and password");
        Dao<User> userDao = new UserDaoImpl();
        List<User> users = userDao.getAll();
        Scanner scanner = new Scanner(System.in);
        String login = scanner.next();
        String password = scanner.next();
        String hash = HashPassword.generateHashMD5(password);
        boolean finishedLoop = true;
        for (User user1 : users) {
            if (user1.getLogin().equals(login) && user1.getPassword().equals(hash)) {
                user.setId(user1.getId());
                user.setLogin(user1.getLogin());
                user.setPassword(user1.getPassword());
                user.setRole(user1.getRole());
                finishedLoop = false;
                System.out.println("you entered the system");
            }
        }
        if (finishedLoop) {
            System.out.println("wrong login and password");
            Run.run();
        }
    }
}
