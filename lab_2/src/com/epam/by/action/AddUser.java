package com.epam.by.action;

import com.epam.by.Run;
import com.epam.by.dao.Dao;
import com.epam.by.dao.daoImpl.UserDaoImpl;
import com.epam.by.entity.Role;
import com.epam.by.entity.User;
import com.epam.by.util.HashPassword;
import com.epam.by.util.ValidateRole;

import java.util.Scanner;

public class AddUser implements UserAction {


    @Override
    public void doAction(User user) {
        Dao<User> dao = new UserDaoImpl();
        if (ValidateRole.validate(user.getRole())) {
            User addingUser = new User();
            Scanner scanner = new Scanner(System.in);
            System.out.println("enter id");
            addingUser.setId(scanner.nextInt());
            System.out.println("enter login");
            addingUser.setLogin(scanner.next());
            System.out.println("enter password");
            String password = scanner.next();
            addingUser.setPassword(HashPassword.generateHashMD5(password));
            System.out.println("enter role[user,admin]");
            String role = scanner.next();
            if (role.equals("user")){
                addingUser.setRole(Role.USER);
            }else if (role.equals("admin")) {
                addingUser.setRole(Role.ADMINISTRATOR);
            }
            dao.add(addingUser);
            System.out.println("user ["+addingUser.toString()+"] successfully added");
            Run.run();
        }else {
            System.out.println("not enough permissions");
            Run.run();
        }
    }

    private StringBuilder generateSalt(String symbols){
        StringBuilder randString = new StringBuilder();
        int count = (int)(Math.random()*30);
        for(int i=0;i<count;i++)
            randString.append(symbols.charAt((int)(Math.random()*symbols.length())));

        return randString;
    }


}
