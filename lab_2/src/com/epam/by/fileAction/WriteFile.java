package com.epam.by.fileAction;

import com.epam.by.dao.Dao;
import com.epam.by.dao.daoImpl.BookDaoImpl;
import com.epam.by.dao.daoImpl.UserDaoImpl;
import com.epam.by.entity.User;
import java.io.*;
import java.util.List;

public class WriteFile {
    public static <T> void writeInfo(String fileName, T input) {
        Dao dao;
        if (input instanceof User) {
            dao = new UserDaoImpl();
        } else {
            dao = new BookDaoImpl();
        }
        List list = dao.getAll();
        FileOutputStream outputStream;
        try {
            outputStream = new FileOutputStream(fileName);
            StringBuilder outputString = new StringBuilder();

            for (Object aList : list) {
                outputString.append(aList.toString()).append("\n");
            }
            outputString.append(input.toString());

            byte[] strToBytes = outputString.toString().getBytes();
            outputStream.write(strToBytes);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <T> void deleteInfo(String fileName, T input) {
        Dao dao;
        if (input instanceof User) {
            dao = new UserDaoImpl();
        } else {
            dao = new BookDaoImpl();
        }
        List list = dao.getAll();
        FileOutputStream outputStream;
        try {
            outputStream = new FileOutputStream(fileName);
            StringBuilder outputString = new StringBuilder();
            list.remove(input);
            for (Object aList : list) {
                outputString.append(aList.toString()).append("\n");
            }
            byte[] strToBytes = outputString.toString().getBytes();
            outputStream.write(strToBytes);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
