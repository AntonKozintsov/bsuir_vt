package com.epam.by.fileAction;

import com.epam.by.constant.Constant;
import com.epam.by.entity.Book;
import com.epam.by.entity.BookType;
import com.epam.by.entity.Role;
import com.epam.by.entity.User;

import java.util.ArrayList;
import java.util.List;

public class CreateEntity {
    public static List<User> getUserList() {
        final List<String> userValues = ReadFile.readFromFile(Constant.USER_FILE);
        final List<User> list = new ArrayList<>();
        for (int i = 0; i < userValues.size(); i = i + 5) {
            User user = new User();
            user.setId(Integer.parseInt(userValues.get(i)));
            user.setLogin(userValues.get(i + 1));
            user.setPassword(userValues.get(i + 2));
            user.setEmail(userValues.get(i+4));
            if (userValues.get(i + 3).equals("user")) {
                user.setRole(Role.USER);

            } else {
                user.setRole(Role.ADMINISTRATOR);
            }
            list.add(user);
        }
        return list;
    }
    public static List<Book> getBookList() {
        List<String> bookValues = ReadFile.readFromFile(Constant.BOOK_FILE);
        List<Book> list = new ArrayList<>();
        for (int i=0;i<bookValues.size();i=i+4) {
            Book book = new Book();
            book.setId(Integer.parseInt(bookValues.get(i)));
            book.setTitle(bookValues.get(i+1));
            book.setAuthor(bookValues.get(i+2));
            if (bookValues.get(i+3).equals("e_book")){
                book.setBookType(BookType.E_BOOK);
            }else if (bookValues.get(i+3).equals("paper_book")) {
                book.setBookType(BookType.PAPER_BOOK);
            }
            list.add(book);
        }
        return list;
    }
}
