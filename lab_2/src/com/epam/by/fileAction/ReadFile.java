package com.epam.by.fileAction;

import com.epam.by.constant.Constant;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReadFile {

    public static List<String> readFromFile(String fileName) {
        List<String> list = new ArrayList<>();
        String space;
        if (fileName.equals(Constant.USER_FILE)){
            space=" ";
        }else {
            space=" {2}";
        }
        try {
            list = Files.lines(Paths.get(fileName))
                    .flatMap(line -> Stream.of(line.split(space)))
                    .collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }
}
