package com.company.servlets;

import com.company.action.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class Controller extends HttpServlet {
    @Override
    public void doGet(final HttpServletRequest req,
                      final HttpServletResponse resp)
            throws ServletException, IOException {
        try {
            this.process(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doPost(final HttpServletRequest req,
                       final HttpServletResponse resp)
            throws ServletException, IOException {
        try {
            this.process(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param req  request.
     * @param resp response.
     * @throws ServletException exception.
     * @throws IOException      exception.
     */
    private void process(final HttpServletRequest req,
                         final HttpServletResponse resp)
            throws ServletException, IOException, SQLException {
        Action action;
        switch (req.getParameter("action")) {
            case "login":
                action = new Authorization();
                action.doAction(req, resp);
                break;
            case "logout":
                action = new LogOut();
                action.doAction(req, resp);
                break;
            case "reservation":
                action = new Reservation();
                action.doAction(req, resp);
                break;
            case "roomsall":
                System.out.println("rooms");
                action = new ShowUsers();
                action.doAction(req, resp);
                break;
            case "show_rooms":
                action = new ShowRooms();
                action.doAction(req, resp);
                break;
            default:
                break;
        }
    }
}
