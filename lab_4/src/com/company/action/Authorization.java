package com.company.action;

import com.company.dao.mysql.UserDaoImpl;
import com.company.entity.User;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Class for user authorization.
 */
public class Authorization implements Action {

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction(final HttpServletRequest req,
                         final HttpServletResponse resp)
            throws IOException, SQLException {
        String message;
        HttpSession session = req.getSession(false);
        UserDaoImpl userDao = new UserDaoImpl();
        String name = req.getParameter("name");
        String password = req.getParameter("password");
        User user = userDao.selectUser(name);
        System.out.println(user);
        if (user.getPassword().equals(password)) {
            message = "you successfully logged in";
            req.getSession().setAttribute("user", user);
            session.setAttribute("loginMessage", message);

            UserRedirect.redirect(req, resp, user.getPermission());
        } else {
            message = "wrong login or password";
            session.setAttribute("loginMessage", message);
            resp.sendRedirect("/index.jsp");
        }
    }

}
