package com.company.action;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Interface for action classes.
 */
public interface Action {
    /**
     * Method to perform action.
     * @param req request
     * @param resp response
     * @throws IOException exception
     * @throws ServletException exception
     */
    void doAction(HttpServletRequest req, HttpServletResponse resp)
            throws IOException, ServletException, SQLException;

}
