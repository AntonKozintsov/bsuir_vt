package com.company.action;

import com.company.entity.Permission;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class to redirect user.
 */
public final class UserRedirect {
    /**
     * private constructor.
     */
    private UserRedirect() {
    }

    /**
     * @param req  request.
     * @param resp response.
     * @param role role.
     */
    public static void redirect(final HttpServletRequest req,
                                final HttpServletResponse resp,
                                final Permission role) {
        System.out.println(role);
        try {
            switch (role) {

                case USER:
                    req.getRequestDispatcher("/user.jsp")
                            .forward(req, resp);
                    break;
                case ADMINISTRATOR:
                    req.getRequestDispatcher("/admin.jsp")
                            .forward(req, resp);
                    break;
                default:
                    break;

            }

        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }

    }
}
