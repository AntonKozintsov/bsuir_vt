package com.company.action;

import com.company.dao.mysql.RoomDaoImpl;
import com.company.entity.Room;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Class to view user subscriptions.
 */
public class ShowRooms implements Action {
    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction(final HttpServletRequest req,
                         final HttpServletResponse resp) {
        String message;
        int id = Integer.parseInt(req.getParameter("user_id"));
        RoomDaoImpl roomDao;
        List<Room> roomList;
        try {
            HttpSession session = req.getSession(false);
            roomDao = new RoomDaoImpl();
            roomList = roomDao.selectRoom(id);
            if (roomList.isEmpty()) {
                message = "you dont have reserved room";
                session.setAttribute("subscriptionMessage", message);
                resp.sendRedirect("/user.jsp");
            } else {
                message = "your subscription list is ready";
                session.setAttribute("subscriptionMessage", message);
                session.setAttribute("rooms", roomList);
                resp.sendRedirect("/user.jsp");
            }
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }

    }
}
