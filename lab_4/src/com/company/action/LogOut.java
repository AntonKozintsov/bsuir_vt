package com.company.action;


import com.company.entity.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class to log out.
 */
public class LogOut implements Action {

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction(final HttpServletRequest req,
                         final HttpServletResponse resp) throws IOException {
        req.getSession().removeAttribute("user");
        req.removeAttribute("user");
        try {
            req.getRequestDispatcher("/index.jsp").forward(req, resp);
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }
}
