package com.company.action;

import com.company.dao.UserDao;
import com.company.dao.mysql.RoomDaoImpl;
import com.company.dao.mysql.UserDaoImpl;
import com.company.entity.Room;
import com.company.entity.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class ShowUsers implements Action {
    @Override
    public void doAction(final HttpServletRequest req,
                         final HttpServletResponse resp) throws SQLException, IOException {

        RoomDaoImpl roomDao = new RoomDaoImpl();
        List<Room> roomList = roomDao.selectAll();
        System.out.println(roomList);
        HttpSession session = req.getSession(false);
        session.setAttribute("roomsall", roomList);
        resp.sendRedirect("/admin.jsp");

    }
}
