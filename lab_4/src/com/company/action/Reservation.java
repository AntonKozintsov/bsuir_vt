package com.company.action;


import com.company.dao.mysql.RoomDaoImpl;
import com.company.dao.mysql.UserDaoImpl;
import com.company.entity.Room;
import com.company.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Class to buy subscription.
 */
public class Reservation implements Action {
    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction(final HttpServletRequest req,
                         final HttpServletResponse resp)
            throws IOException {
        HttpSession session = req.getSession(false);
        String message;
        UserDaoImpl userDao = new UserDaoImpl();
        RoomDaoImpl roomDao = new RoomDaoImpl();
        List<Room> roomList = null;
        try {
            roomList = roomDao.selectAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        User user = (User) req.getSession().getAttribute("user");
        if (roomList != null) {
            for (Room aRoomList : roomList) {
                if (!aRoomList.isChecked()) {
                    try {
                        userDao.updateRoom(user.getName());
                        roomDao.updateRoom(user.getId(), aRoomList.getId());
                        message="something goes wrong";
                        session.setAttribute("message", message);
                        resp.sendRedirect("/user.jsp");
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }
        message="something goes wrong";
        session.setAttribute("message", message);
        resp.sendRedirect("/user.jsp");
    }
}
