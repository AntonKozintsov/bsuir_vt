package com.company.entity;

import java.util.Objects;

public class User {
    private int id;
    private Permission permission;
    private String name;
    private boolean isChecked;
    private String password;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return id == user.id &&
                isChecked == user.isChecked &&
                permission == user.permission &&
                Objects.equals(name, user.name) &&
                Objects.equals(password, user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, permission, name, isChecked, password);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", permission=" + permission +
                ", name='" + name + '\'' +
                ", isChecked=" + isChecked +
                ", password='" + password + '\'' +
                '}';
    }
}
