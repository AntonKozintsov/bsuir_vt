package com.company.entity;

import java.util.Objects;

public class Room {


    private int id;
    private boolean isChecked;
    User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", isChecked=" + isChecked +
                ", user=" + user +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Room)) return false;
        Room room = (Room) o;
        return id == room.id &&
                isChecked == room.isChecked &&
                Objects.equals(user, room.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, isChecked, user);
    }
}
