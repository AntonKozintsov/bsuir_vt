package com.company.entity;

public enum Permission {
    ADMINISTRATOR,
    USER;
}
