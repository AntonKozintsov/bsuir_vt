package com.company.dao;

import com.company.entity.User;

import javax.management.relation.Role;
import java.sql.SQLException;
import java.util.List;

public interface UserDao {
    boolean isExist(String name, String password) throws SQLException;
    User selectUser(String name) throws SQLException;
    List<User> selectAll() throws SQLException;
    User selectUser(int id) throws SQLException;
}
