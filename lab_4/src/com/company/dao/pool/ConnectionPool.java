package com.company.dao.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Connection pool.
 */
public final class ConnectionPool {

    /**
     * Value of the object ConnectionPool.
     */
    private static ConnectionPool instance;

    /**
     * Value of the object ReentrantLocker.
     */
    private static ReentrantLock locker;

    /**
     * Queue of the connections.
     */
    private BlockingQueue<Connection> connections;

    static {
        locker = new ReentrantLock();
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Private default constructor.
     */
    private ConnectionPool() {
        this.connections = new LinkedBlockingQueue<>();
    }

    /**
     * This method return value of the single object ConnectionPool.
     *
     * @return value of the object ConnectionPool
     */
    public static ConnectionPool getInstance() {
        locker.lock();
        try {
            if (instance == null) {
                instance = new ConnectionPool();
            }
        } finally {
            locker.unlock();
        }
        return instance;
    }

    /**
     * @return connection.
     */
    public Connection getConnection() {
        Connection connection = null;
        locker.lock();
        try {
            while (connection == null) {
                try {
                    if (this.connections.isEmpty()) {
                        connection = DriverManager.getConnection(
                                "jdbc:mysql://localhost:3306/hoteldb?useSSL=false",
                                "root",
                                "myrootpass_5D");
                    } else {
                        connection = this.connections.take();
                        if (!connection.isValid(0)) {
                            connection = null;
                        }
                    }
                } catch (InterruptedException | SQLException e) {
                    e.printStackTrace();
                }
            }
        } finally {
            locker.unlock();
        }
        return connection;
    }

}
