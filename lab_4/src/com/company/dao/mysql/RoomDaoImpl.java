package com.company.dao.mysql;

import com.company.dao.RoomDao;
import com.company.dao.pool.ConnectionPool;
import com.company.entity.Room;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
public class RoomDaoImpl implements RoomDao {

    public List<Room> selectRoom(final int userId) throws SQLException {
        List<Room> roomList = new ArrayList<>();
        String sql = "SELECT * FROM `room` o WHERE o.`user_id` = ?";
        final ResultSet resultSet;
        Connection connection = null;
        PreparedStatement statement = null;
        Room room;

        try {
            connection = ConnectionPool.getInstance().getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, userId);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                room = create(resultSet);
                roomList.add(room);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            statement.close();
            connection.close();

        }

        return roomList;
    }
    public void updateRoom(int userId, int roomId) throws SQLException {
        String sql = "UPDATE room SET is_checked = ?, user_id = ? where id = ?";
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setBoolean(1, true);
            preparedStatement.setInt(2, userId);
            preparedStatement.setInt(3, roomId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
    //        String sql = "UPDATE room SET is_checked = ?, user_id = ? where id = ?";

    public List<Room> selectAll() throws SQLException {
        String sql = "SELECT * FROM `room`";
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        List<Room> roomList = new ArrayList<>();
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(
                    sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                roomList.add(create(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connection.close();
            preparedStatement.close();
        }
        return roomList;
    }


    private Room create(final ResultSet resultSet) {
        Room room = new Room();
        UserDaoImpl userDao = new UserDaoImpl();
        if (resultSet != null) {
            try {
                room.setId(resultSet.getInt("id"));
                room.setUser(userDao.selectUser(resultSet.getInt("user_id")));
                room.setChecked(resultSet.getBoolean("is_checked"));
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return room;
    }
}
