package com.company.dao.mysql;

import com.company.dao.UserDao;
import com.company.dao.pool.ConnectionPool;
import com.company.entity.Permission;
import com.company.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
public class UserDaoImpl implements UserDao {
    @Override
    public boolean isExist(String name, String password) throws SQLException {
        String sql = "SELECT * FROM `user` WHERE `name` = ? "
                + "AND `password` = ?";
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        final ResultSet resultSet;
        boolean result = false;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(
                    sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                result = true;
            }


        } catch (SQLException e) {

            e.printStackTrace();


        } finally {
            if (connection != null) {
                connection.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }

        return result;

    }

    @Override
    public User selectUser(final String name) throws SQLException {
        String sql = "SELECT * FROM `user` WHERE name = ?";
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        final ResultSet resultSet;
        User user = new User();
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(
                    sql);
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                user = create(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connection.close();
            preparedStatement.close();

        }
        return user;
    }

    private User create(ResultSet resultSet) {
        User user = new User();
        if (resultSet != null) {
            try {

                user.setId(resultSet.getInt("id"));
                user.setPassword(resultSet.getString("password"));
                user.setName(resultSet.getString("name"));
                user.setChecked(resultSet.getBoolean("is_checked"));
                switch (resultSet.getString("permission")) {
                    case "0":
                        user.setPermission(Permission.USER);
                        break;
                    case "1":
                        user.setPermission(Permission.ADMINISTRATOR);
                        break;
                    default:
                        break;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return user;
    }

    @Override
    public List<User> selectAll() throws SQLException {
        String sql = "SELECT * FROM `user`";
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        List<User> userList = new ArrayList<>();
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(
                    sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                userList.add(create(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return userList;
    }


    @Override
    public User selectUser(int id) throws SQLException {
        String sql = "SELECT * FROM `user` WHERE id = ?";
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        final ResultSet resultSet;
        User user = new User();
        try {
            connection =  ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(
                    sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                user = create(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }

        }
        return user;
    }
    public void updateRoom(final String name) throws SQLException {
        String sql = "UPDATE `user` u SET u.`is_checked` "
                + "= ? WHERE u.`name` = ?";
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        User user = this.selectUser(name);
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setBoolean(1, true);
            preparedStatement.setString(2, user.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
    }
}
