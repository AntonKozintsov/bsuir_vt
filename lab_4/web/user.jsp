<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <%--<link rel="stylesheet" type="text/css" href="website/css/style.css">--%>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/website/css/style.css"/>
    <!-- Bootstrap CSS -->
    <script type="text/javascript">
        <%@include file="/website/js/scripts.js"%>
    </script>
    <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
    <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js'></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Fitness</title>
</head>
<body>
<div class="userInf">
    <c:set var="user" value="${user}"/>
    <div class="text userName"><c:out value="${user.getName()}"/></div>
    <form method="post" action="hotel">
        <button name="action" value="logout" class="btn btn-primary logout">LOGOUT</button>
        <button name="action" value="show_rooms" class="btn btn-primary logout">SHOW ROOMS</button>
        <input type="hidden" name="user_id" value="${user.getId()}">
    </form>
</div>

<form method="post" action="hotel">
    <button type="submit" name="action" value="reservation">Buy room</button>
    <c:if test="${sessionScope.message != null}">
        <script>
            var orderMessage = "${sessionScope.message}";
            orderAlert(orderMessage);
        </script>
        ${sessionScope.message=null};
    </c:if>
</form>
<c:if test="${sessionScope.subscriptionMessage != null}">
    <script>
        var subscriptionMessage = "${sessionScope.subscriptionMessage}";
        subscriptionAlert(subscriptionMessage);
        ${sessionScope.subscriptionMessage=null}
    </script>
</c:if>
<c:if test="${sessionScope.rooms != null}">
    <div class="conteiner-fluid sectin2">
        <div class="row">
            <div class="col-md-12 text-center heading-wrap">
                <h2>Your rooms</h2>
            </div>
        </div>
    </div>
    <div class="midtable">
        <table>
            <thead>
            <tr>
                <th><c:out value="id"/></th>
                <th><c:out value="is_checked"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="room" items="${sessionScope.rooms}">
                <tr>
                    <td><c:out value="${room.getId()}"/></td>
                    <td><c:out value="${room.isChecked()}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    ${sessionScope.rooms=null}
</c:if>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</body>
</html>